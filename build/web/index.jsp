<%-- 
    Document   : index
    Created on : Apr 9, 2016, 2:32:53 PM
    Author     : Emil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-color:${bc}">
        <h1>Hello World!</h1>
        
        <form action="NewServlet" method="POST">
            Select Background Color
            <select name="color" size="1">
                <option value="red">red</option>
                <option value="green">green</option>
                <option value="blue">blue</option>
                <input type="submit">
            </select>
    </body>
</html>
